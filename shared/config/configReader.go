package config

import (
	"log"
	"os"
	"strings"
)

type VPNConfig struct {
	NmcliConnectionID string
	Location          string
	ImageName         string
	Token             string
	Variant           string
	SSHKeyFingerprint string
}

type ClientConfig struct {
	Username        string
	ServerURL       string
	AccessToken     string
	Nmcli           string
	NmcliID         string
	NmcliPassword   string
	OpenVPNTemplate string
	OpenVPNTarget   string
	OpenVPNstartCmd string
	OpenVPNstopCmd  string
}

var configDirectory = "/etc/gopherportal/"
var configServerFileName = "server.conf"
var configClientFileName = "gophclient.conf"

/*
//Read from config file
var NmcliConnectionID = "'chris'"
var Location = "sfo1"
var ImageName = "VPN Snapshot"
var Token = "12345A"
*/

//Reads server config from configServerFileName
func ReadServerConfig() *VPNConfig {

	var readConfig VPNConfig
	var fileMode os.FileMode
	var file *os.File
	var newFile = false

	file, err := os.OpenFile(configDirectory+configServerFileName, os.O_RDONLY, fileMode)

	if os.IsNotExist(err) {
		log.Printf(err.Error())
		file, err = os.Create(configServerFileName)
		newFile = true

		if err != nil {
			log.Fatal(err)
		}
	}

	if !newFile {

		fileInfo, _ := file.Stat()
		var data = make([]byte, fileInfo.Size())
		_, err := file.Read(data)

		if err != nil {
			log.Fatal(err)
		}

		lines := string(data)
		configs := strings.Split(lines, "\n")

		readConfig.NmcliConnectionID = strings.SplitN(configs[0], "=", 2)[1]
		readConfig.Location = strings.SplitN(configs[1], "=", 2)[1]
		readConfig.ImageName = strings.SplitN(configs[2], "=", 2)[1]
		readConfig.Token = strings.SplitN(configs[3], "=", 2)[1]
		readConfig.Variant = strings.SplitN(configs[4], "=", 2)[1]
		readConfig.SSHKeyFingerprint = strings.SplitN(configs[5], "=", 2)[1]

		file.Close()

		return &readConfig
	}

	file.Close()
	return nil

}

//Reads client config from configClientFileName
func ReadClientConfig() *ClientConfig {

	var readConfig ClientConfig
	var fileMode os.FileMode
	var file *os.File
	var newFile = false

	file, err := os.OpenFile(configDirectory+configClientFileName, os.O_RDONLY, fileMode)

	if os.IsNotExist(err) {
		log.Printf(err.Error())
		file, err = os.Create(configClientFileName)
		newFile = true

		if err != nil {
			log.Fatal(err)
		}
	}

	if !newFile {

		fileInfo, _ := file.Stat()

		var data = make([]byte, fileInfo.Size())

		_, err := file.Read(data)

		if err != nil {
			log.Fatal(err)
		}
		lines := string(data)

		configs := strings.Split(lines, "\n")

		readConfig.Username = strings.SplitN(configs[0], "=", 2)[1]
		readConfig.ServerURL = strings.SplitN(configs[1], "=", 2)[1]
		readConfig.AccessToken = strings.SplitN(configs[2], "=", 2)[1]
		readConfig.Nmcli = strings.SplitN(configs[3], "=", 2)[1]
		readConfig.NmcliID = strings.SplitN(configs[4], "=", 2)[1]
		readConfig.NmcliPassword = strings.SplitN(configs[5], "=", 2)[1]
		readConfig.OpenVPNTemplate = strings.SplitN(configs[6], "=", 2)[1]
		readConfig.OpenVPNTarget = strings.SplitN(configs[7], "=", 2)[1]
		readConfig.OpenVPNstartCmd = strings.SplitN(configs[8], "=", 2)[1]
		readConfig.OpenVPNstopCmd = strings.SplitN(configs[9], "=", 2)[1]

		file.Close()

		return &readConfig
	}

	file.Close()
	return nil
}

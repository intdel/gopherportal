package web

//New feature ideas: select location, select image, history of actions, show droplet action time in status etc

import (
	"errors"
	"fastnet/server/database"
	"fastnet/shared/config"
	"log"
	"strconv"
	"strings"


	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
	"net/http"
	"github.com/abbot/go-http-auth"
)

//Internal vars
var imageID int
var vpnIP string
var vpnID int
var Version = "1.7.3 WEB Build 02/03/17"

//Read from config file
var vpnConfig config.VPNConfig
var nmcliConnectionID string
var location string
var imageName string
var token string
var configSet = false
var sessionClient *godo.Client
var sshFingerprint string

//For DigitalOcean API calls
type TokenSource struct {
	AccessToken string
}

func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

/*
create -- create new VPNclient Droplet Ok
delete -- delete VPN droplet if it exists OK
images -- list snapshots OK
  list -- list all droplets OK
 nmcli -- Print nmcli command OK
status -- check VPN droplet status OK

*/


//TODO It's temporary for the IP request since it's still using basic http auth
//Handler accepts requests here and evaluates choices (called by http serve thing)
func AuthHandler(r *auth.AuthenticatedRequest) string {

	//Take all the stuff from the request
	//Have to do it like this to remove /portal/*request*, but also be able to handle stuff like /ip
	choice := r.URL.Path[1:]
	choice = strings.Replace(choice, "portal/", "", -1)
	//username + ip were used for history, but removed due to DEPRECATED
	//username, _, _ := r.BasicAuth()
	//ip := r.Header.Get("X-Forwarded-For")

	//Get DO client
	prepareClient()

	//If there is no image, then there is no point to try to do the other things
	if findVPNImageID(sessionClient) == false {
		return "Image not available at the moment. Please try again later."
	}

	//Add any choice to history *DISABLED DUE TO DEPRECATED*
	//addHistory(choice, username, ip)

	//Clients still in argument, can be removed in the future, since moved "client" to global
	switch choice {
	case "precreate":
		return queryDropletHours(sessionClient)
	case "create":
		return createVPNDroplet(sessionClient, r.FormValue("hours"))
	case "delete":
		return destroyVPNDroplet()
	case "status":
		return printStatus(sessionClient)
	case "list":
		return printDroplets(sessionClient)
	case "images":
		return printImages(sessionClient)
	case "ip":
		return printVPNIP(sessionClient)
	case "renew":
		return renewDroplet(sessionClient, 2)
	case "help":
		return printHelp()
	case "":
		return printHelp()
	}

	return "Uh Oh!"

}

//Handler accepts requests here and evaluates choices (called by http serve thing)
func Handler(r *http.Request) string {

	//Take all the stuff from the request
	//Have to do it like this to remove /portal/*request*, but also be able to handle stuff like /ip
	choice := r.URL.Path[1:]
	choice = strings.Replace(choice, "portal/", "", -1)
	//username + ip were used for history, but removed due to DEPRECATED
	//username, _, _ := r.BasicAuth()
	//ip := r.Header.Get("X-Forwarded-For")

	//Get DO client
	prepareClient()

	//If there is no image, then there is no point to try to do the other things
	if findVPNImageID(sessionClient) == false {
		return "Image not available at the moment. Please try again later."
	}

	//Add any choice to history *DISABLED DUE TO DEPRECATED*
	//addHistory(choice, username, ip)

	//Clients still in argument, can be removed in the future, since moved "client" to global
	switch choice {
	case "precreate":
		return queryDropletHours(sessionClient)
	case "create":
		return createVPNDroplet(sessionClient, r.FormValue("hours"))
	case "delete":
		return destroyVPNDroplet()
	case "status":
		return printStatus(sessionClient)
	case "list":
		return printDroplets(sessionClient)
	case "images":
		return printImages(sessionClient)
	case "ip":
		return printVPNIP(sessionClient)
	case "renew":
		return renewDroplet(sessionClient, 2)
	case "help":
		return printHelp()
	case "":
		return printHelp()
	}

	return "Uh Oh!"

}

//Returns a form that asks for how many hours to create droplet before creating (default = 4)
// Uses "hours" formValue for /create
func queryDropletHours(client *godo.Client) string {
	//Avoid creating multiple VPN droplets. We only need one
	if findVPNDropletID(client) != -1 {
		log.Println(errors.New("Create fail. VPN Droplet already exists."))
		return "VPN droplet already exists. Will not create a new one."
	}

	selectString := ""

	for i := -1; i < 10; i++ {

		if i == 4 {
			selectString += "<option value=" + strconv.Itoa(i) + " selected>" + strconv.Itoa(i) + " hrs. </option>\n"
		} else {
			selectString += "<option value=" + strconv.Itoa(i) + ">" + strconv.Itoa(i) + " hrs. </option>\n"
		}
	}

	outputstring := "<p>Create VPN Droplet for certain hours. Droplet will be deleted once it reaches 0. Use -1 for infinite.</p>" +
		"<form action=\"/portal/create\" method=\"get\"><p>For how many hours:" +
		"<select name=hours>" + selectString +
		"</select>" +
		" <button class=\"btn-success btn-sm\" type=\"submit\">Create VPN</button></p></form>"
	return outputstring

}

//Creates a VPN Droplet from the image and returns HTML result
func createVPNDroplet(client *godo.Client, hours string) string {

	//Avoid creating multiple VPN droplets. We only need one
	if findVPNDropletID(client) != -1 {
		log.Println(errors.New("Create fail. VPN Droplet already exists."))
		return "VPN droplet already exists. Will not create a new one."
	}

	var dropletCreateImage godo.DropletCreateImage
	dropletCreateImage.ID = imageID


	var dropletSSHKeyArray []godo.DropletCreateSSHKey = make([]godo.DropletCreateSSHKey, 1)
	dropletSSHKeyArray[0].Fingerprint = sshFingerprint

	var dropletCreateRequest godo.DropletCreateRequest
	dropletCreateRequest.Name = "VPN-" + strconv.Itoa(imageID)
	dropletCreateRequest.Region = location
	dropletCreateRequest.Size = "512mb"
	dropletCreateRequest.Image = dropletCreateImage
	dropletCreateRequest.SSHKeys = dropletSSHKeyArray


	droplet, resp, err := client.Droplets.Create(&dropletCreateRequest)

	if err != nil {
		log.Println("Something went wrong while creating droplet")
		log.Printf("Response is: %s\n", resp.Status)
		log.Printf(err.Error())
		return err.Error()
	}

	outputString := "<p>Response is: " + resp.Status + "</p>\n"
	outputString += "<p>Droplet " + droplet.Name + " created with ID " + strconv.Itoa(droplet.ID) + ".</p><p>It is now in status <a class=\"btn-sm btn-warning\" href=\"#\"> " + droplet.Status + "</a>.</p><p>Please wait a few minutes until it reaches <a class=\"btn-sm btn-success\" href=\"#\">active</a></p><p>Redirecting to <a class=\"btn-sm\" href=\"status\">status</a> page in a few seconds<meta http-equiv=\"refresh\" content=\"10; url=status\" />!</p>\n"

	log.Printf("Response is: %s\n", resp.Status)
	log.Printf("Droplet %s created with ID %d. It is now in status \"%s\".\nPlease wait a few minutes until it reaches \"active\"...\n", droplet.Name, droplet.ID, droplet.Status)

	//Adding droplet to database
	hoursInt, err := strconv.Atoi(hours)
	if err != nil {
		log.Println("ERROR while converting hours to integer!")
		hoursInt = -1
	}

	log.Printf("Adding droplet %d with %d hours to database...\n", droplet.ID, hoursInt)
	database.AddDroplet(droplet.ID, hoursInt)
	createMonitor(droplet.ID)
	return outputString
}

//Prints the button and that type of stuff
func printHelp() string {
	return "<table width=\"30%\" align=\"center\"><tr height=\"50\" align=\"center\"><td><a class=\"btn btn-primary\" href=\"precreate\">Create VPN</a></td><td><a class=\"btn btn-danger\" href=\"delete\">Delete VPN</a></td></tr><tr height=\"50\" align=\"center\"><td><a class=\"btn btn-success\" href=\"images\">List Images</a></td><td><a class=\"btn btn-success\" href=\"list\">List Droplets</a></td></tr><tr height=\"50\" align=\"center\"><td><a class=\"btn btn-warning\" href=\"status\">Check Status</a></td><td><a class=\"btn btn-danger\" href=\"/stop\">Stop server</a></td></tr><tr height=\"50\" align=\"center\"><td><a class=\"btn btn-primary\" href=\"renew\">Add 2 Hours</a></td><td>&nbsp;</td></tr></table>"

}

//List images (returns HTML table of images)
func printImages(client *godo.Client) string {
	opt := godo.ListOptions{}
	images, resp, err := client.Images.ListUser(&opt)

	if err != nil {
		log.Println(resp.Body)
		log.Println("Something went wrong while trying to get images")
		log.Println(err)
		return "Something went wrong while trying to get images. " + err.Error()
	}

	outputString := "<table border=\"1\" width=\"50%\" align=\"center\"><tr align=\"center\"><td>Name</td><td>ID</td></tr>\n"

	for i := range images {
		image := images[i]
		outputString += "<tr align=\"center\"><td>" + image.Name + "</td><td>" + strconv.Itoa(image.ID) + "</td></tr>\n"
	}

	outputString += "</table>"

	return outputString

}

//Internally used to find the Droplet's image ID. If not found, returns false. Will set global var imageID
func findVPNImageID(client *godo.Client) bool {
	opt := godo.ListOptions{}
	images, resp, err := client.Images.ListUser(&opt)

	if err != nil {
		log.Println(resp.Body)
		log.Println("Something went wrong while trying to get image ID")
		return false
	}

	for i := range images {
		image := images[i]
		if image.Name == imageName {
			imageID = image.ID
		}
	}

	if imageID != 0 {
		return true
	}
	return false

}

//Internally used to find the Droplet. If not found, returns nil.
func findDroplet(client *godo.Client) *godo.Droplet {
	dropletID := findVPNDropletID(client)
	if dropletID != -1 {
		droplet, _, err := client.Droplets.Get(dropletID)
		if err != nil {
			log.Println(errors.New("Something went wrong while retrieving the Droplet"))
			log.Println(err)
			return nil
		}
		return droplet
	}

	return nil
}

//Internally used to find the Droplet's ID. If not found, returns -1 else the ID. Will set global var vpnID and vpnIP
//Technically we would have to scan multiple pages, but since we only have up to 2 droplets, I do not bother with that
func findVPNDropletID(client *godo.Client) int {
	opt := &godo.ListOptions{}
	droplets, _, err := client.Droplets.List(opt)

	if err != nil {
		log.Println("Something went wrong while trying to get Droplet ID")
		log.Println(err)
		return -1
	}

	for d := range droplets {
		tempDroplet := droplets[d]
		if tempDroplet.Name == "VPN-"+strconv.Itoa(imageID) {
			vpnID = tempDroplet.ID
			vpnIP, _ = tempDroplet.PublicIPv4()
			return tempDroplet.ID
		}
	}

	return -1
}

//Returns status (e.g. active, new, etc.) of Droplet in HTML format
func printStatus(client *godo.Client) string {

	dropletID := findVPNDropletID(client)

	if dropletID == -1 {
		log.Println("Failed printing status: VPN Droplet does not exist")
		return "VPN droplet does not exist."
	}

	droplet, resp, err := client.Droplets.Get(dropletID)

	if err != nil {
		log.Println(err)
		return "Something went wrong while getting droplet: " + resp.Status
	}

	if droplet.Status != "active" {

		return droplet.Name + " is in <a class=\"btn-sm btn-warning\" href=\"#\">" + droplet.Status + "</a><p>Reloading every 10 seconds until it reaches <a class=\"btn-sm btn-success\" href=\"#\">active</a><meta http-equiv=\"refresh\" content=\"10; url=status\" /></p><p><a class=\"btn-sm btn-primary\" href=\"status\">Check now</a></p>"
	}

	return droplet.Name + " is in <a class=\"btn-sm btn-success\" href=\"#\">" + droplet.Status + "</a><p>"
}

//List droplets in HTML table
func printDroplets(client *godo.Client) string {
	opt := &godo.ListOptions{}
	droplets, _, err := client.Droplets.List(opt)

	if err != nil {
		log.Println("Something went wrong while trying to print Droplets")
		log.Println(err)
		return "Something went wrong while trying to print Droplets"
	}
	outputString := "<p><table border=\"1\" width=\"50%\" align=\"center\"><tr align=\"center\"><td>Name</td><td>ID</td><td>Region</td><td>IPv4</td><td>Status</td><td>Rem. Hours</td></tr>\n"

	log.Printf("%12s %8s %15s %6s\n", "Name", "ID", "IPv4", "Status")
	for d := range droplets {
		tempDroplet := droplets[d]
		ipv4, err := tempDroplet.PublicIPv4()
		if err != nil {
			log.Println("Something went wrong while trying to get the IP")
			log.Println(err)
			return "Something went wrong while trying to get the IP"
		}
		log.Printf("%12s %8d %15s %6s\n", tempDroplet.Name, tempDroplet.ID, ipv4, tempDroplet.Status)
		outputString += "<tr align=\"center\"><td>" + tempDroplet.Name + "</td><td>" + strconv.Itoa(tempDroplet.ID) + "</td><td>" + tempDroplet.Region.Name + "</td><td>" + ipv4 + "</td><td>" + tempDroplet.Status + "</td><td>" + strconv.Itoa(database.GetDropletHour(tempDroplet.ID)) + "</td></tr>\n"
	}

	outputString += "</table></p><p><a class=\"btn-sm btn-primary\" href=\"list\">Refresh</a></p>"

	return outputString
}

//Sends a destroy request to Digital Ocean
func destroyVPNDroplet() string {
	dropletID := findVPNDropletID(sessionClient)

	if dropletID != -1 {
		sessionClient.Droplets.Delete(dropletID)
		log.Println("Droplet deleted.")
		return "Droplet removal submitted."
	} else {
		log.Println("Could not find VPN droplet to delete.")
		return "Could not find VPN droplet to delete."
	}

}

//Used by the front end clients to retrieve the IP (-1 means either not found or error)
func printVPNIP(client *godo.Client) string {

	if findVPNDropletID(client) != -1 {
		return vpnIP
	}
	return "-1"

}


//Renews droplet by addHours and creates a monitor
func renewDroplet(client *godo.Client, addHours int) string {

	if findVPNDropletID(client) == -1 {
		return "There is no droplet to renew."
	}

	currentHours := database.GetDropletHour(vpnID)
	log.Printf("Renewing %d from %d to %d\n", vpnID, currentHours, currentHours+addHours)
	database.UpdateDropletHour(vpnID, addHours)

	//monitorinterface.CreateMonitor(vpnID)
	createMonitor(vpnID)

	return "Droplet with ID " + strconv.Itoa(vpnID) + " has been renewed from " + strconv.Itoa(currentHours) + " to " + strconv.Itoa(currentHours+addHours) + "."

}

//Prepares DO client and also sets global sessionClient variable
func prepareClient() *godo.Client {
	if !configSet {
		readConfig := config.ReadServerConfig()
		if readConfig == nil {
			log.Fatal(errors.New("Could not read config file. Cannot continue without configs"))
		}
		vpnConfig = *readConfig

		nmcliConnectionID = vpnConfig.NmcliConnectionID
		location = vpnConfig.Location
		imageName = vpnConfig.ImageName
		token = vpnConfig.Token
		configSet = true
		sshFingerprint = vpnConfig.SSHKeyFingerprint

	}

	//For DO api
	tokenSource := &TokenSource{
		AccessToken: token,
	}

	oauthClient := oauth2.NewClient(oauth2.NoContext, tokenSource)
	sessionClient = godo.NewClient(oauthClient)

	return sessionClient

}

package web

import (
	"fastnet/server/database"
	"log"
	"sync"
	"time"
)

var monitorRunning = false
var mutex = &sync.Mutex{}

//Spawns a monitor thread by default
func createMonitor(id int) {

	mutex.Lock()

	if monitorRunning == false {
		log.Println("Creating time thread...")
		monitorRunning = true
		go monitor(id)
	}

	mutex.Unlock()

}

//Reduces the time of droplet with ID by 1 every hour and eventually deletes it
func monitor(id int) {

	currentHours := database.GetDropletHour(id)

	for currentHours > 0 {
		time.Sleep(time.Hour)

		if currentHours > 0 {
			log.Println("Reducing by 1 hour...")
			database.UpdateDropletHour(id, -1)
			currentHours--
		}

		if currentHours == 0 {
			log.Println("Deleting droplet...")
			destroyVPNDroplet()
		}

		currentHours = database.GetDropletHour(id)
	}
	log.Println("Monitor exiting")
	mutex.Lock()
	monitorRunning = false
	mutex.Unlock()

}

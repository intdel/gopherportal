package credential

import (
	"log"
	_ "github.com/mattn/go-sqlite3"
	"fastnet/server/database"
)


var sessionTableName = database.SessionTableName
var userTableName = database.UserTableName


//Adds a user + session to database
func addSessionToDB(username string, id string) {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("insert into " + sessionTableName + " values(?, ?);")
	defer stmt.Close()


	_, err = stmt.Exec(username, id)
	if err != nil {
		log.Fatalf("Could not insert into " + sessionTableName + " table (" + err.Error() + ")")
	}


}

//Get session from DB
func sessionExistsInDB(username string, id string) bool {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("select * from " + sessionTableName + " where username=? and id=?;")
	defer stmt.Close()

	rows, err := stmt.Query(username, id)
	if err != nil {
		log.Fatalf("Could not select session id (" + err.Error() + ")")
	}


	defer rows.Close()

	for rows.Next(){
		var usernameFromDB string
		var sessionidFromDB string
		err := rows.Scan(&usernameFromDB, &sessionidFromDB)

		if err != nil {
			log.Fatal(err)
		}
		if username == usernameFromDB && sessionidFromDB == id {
			log.Println(username + "logged in successfully (reuse session)")
			return true
		}
	}

	return false
}

//Removes session from DB
func deleteSessionFromDB(username string, id string) {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("delete from " + sessionTableName + " where username=? and id=?;")
	defer stmt.Close()

	_, err = stmt.Exec(username, id)
	if err != nil {
		log.Fatalf("Could not delete session id (" + err.Error() + ")")
	}

}

//Adds a user + password to database
func addUserToDB(username string, password string) {

	if (!userExistsInDB(username)) {

		db := database.OpenDB()
		defer db.Close()

		stmt, err := db.Prepare("insert into " + userTableName + " values(?, ?);")
		defer stmt.Close()


		_, err = stmt.Exec(username, password)
		if err != nil {
			log.Fatalf("Could not insert into " + sessionTableName + " table (" + err.Error() + ")")
		}
		log.Println("Added " + username + " to database!")
	}

}

//Check if user already exists since we only want one user in the db
func userExistsInDB(username string) bool {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("select username from " + userTableName + " where username=?;")
	defer stmt.Close()

	rows, err := stmt.Query(username)
	if err != nil {
		log.Fatalf("Could not select login data (" + err.Error() + ")")
	}

	defer rows.Close()

	for rows.Next(){
		var usernameFromDB string
		err := rows.Scan(&usernameFromDB)

		if err != nil {
			log.Fatal(err)
		}
		if username == usernameFromDB {
			log.Println("Username " + username + " already exists. Not adding!")
			return true
		}
	}

	return false
}

//Check if login is valid in DB
func loginValidInDB(username string, password string) bool {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("select * from " + userTableName + " where username=? and password=?;")
	defer stmt.Close()

	rows, err := stmt.Query(username, password)
	if err != nil {
		log.Println("Could not select login data (" + err.Error() + ")")
		return false
	}

	defer rows.Close()

	for rows.Next() {
		var usernameFromDB string
		var passwordFromDB string
		err := rows.Scan(&usernameFromDB, &passwordFromDB)

		if err != nil {
			log.Fatal(err)
		}
		if username == usernameFromDB && passwordFromDB == password {
			log.Println(username + "logged in successfully")
			return true
		}
	}
	return false
}

//Deletes user from DB
func deleteUserFromDB(username string) {

	db := database.OpenDB()
	defer db.Close()

	stmt, err := db.Prepare("delete from " + sessionTableName + " where username=?;")
	defer stmt.Close()

	_, err = stmt.Exec(username)
	if err != nil {
		log.Fatalf("Could not delete login data (" + err.Error() + ")")
	}

}


package credential

import (
	"io/ioutil"
	"log"
	"strings"
	"crypto/rand"
	"encoding/base64"
)

//Determines if username and password are valid (based on bcrypt)
//TODO somehow encrypt password
//TODO create credential command line tool (maybe even in main.go with parameters) to store this in data.sql
//Right now it adds users from auth.dat (delete after adding)
//PW change currently not supported
func LoginIsValid(username string, password string) bool {
	addUsersFromFile()
	if loginValidInDB(username, password){
		return true
	}

	log.Println(username + ":" + password + " not valid")
	return  false
}

//Generates a unique credential ID for database and cookie
func GenerateSessionID(username string) string {
	b := make([]byte, 1024)
	rand.Read(b)
	randomStr:= base64.URLEncoding.EncodeToString(b)
	saveSessionID(username, randomStr)
	return randomStr
}

//Adds users and password from auth.dat to database
func addUsersFromFile() {
	dat, err := ioutil.ReadFile("auth.dat")
	if err != nil {
		log.Println(err)
	} else
	{
		data := string(dat)
		logins := strings.Split(data, "\n")

		for i := 0; i < len(logins); i++ {
			if strings.Contains(logins[i], ";") {
				loginData := strings.Split(logins[i], ";")
				username := loginData[0]
				password := loginData[1]
				addUserToDB(username, password)
			}
		}
	}
}

func saveSessionID(username string, id string){
	addSessionToDB(username, id)
}

//Deletes session ID from database
func DeleteSessionID(username string, id string){
	deleteSessionFromDB(username, id)
}

//Determines if a username + session ID is valid or not
//If file does not exist, all sessions will be invalid
func SessionIDIsValid(username string, id string) bool {
	return sessionExistsInDB(username, id)
}

#!/bin/bash
go build . && mv server gopherportal && rm -f gopherportal.zip && zip gopherportal.zip gopherportal && scp gopherportal.zip chris@drbachner.de:/home/chris/gopherportal
ssh chris@drbachner.de "killall gopherportal; cd ~/gopherportal; rm gopherportal; unzip gopherportal.zip; nohup ./gopherportal > foo.out 2> foo.err < /dev/null &"

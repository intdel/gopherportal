package main

import (
	"bufio"
	"fastnet/server/web"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	"fastnet/server/credential"

	auth "github.com/abbot/go-http-auth"
	"github.com/gorilla/sessions"
)

//TODO change this to something more secret?
var store = sessions.NewCookieStore([]byte("gopherportal"))

//Handles /portal/* as well as login form
func portalHandler(w http.ResponseWriter, r *http.Request) {

	session := getSession(r)

	if session.Values["username"] == nil || session.Values["sessionid"] == nil{
		//The replacement is because otherwise it won't have the correct path to the images etc.
		//Forgot why
		session.Options.MaxAge = -1
		session.Save(r ,w)
		fmt.Fprintf(w, strings.Replace(readHtmlFile("<center>This area requires authentication. Please log in.<br><br><form action=\"/login\" method=\"post\">Username: <input type=\"text\" class=\"input input-sm\" size=35 name=\"username\"><br><br> Password: <input type=\"password\" size=35 class=\"input input-sm\" name=\"password\"><br><br><button class=\"btn-sm btn-primary\" type=\"submit\">Sign In</button></form></center>", " Welcome to GOpher Portal!", false), "html/", "../html/", -1))
	} else {
		username := session.Values["username"].(string)
		id := session.Values["sessionid"].(string)

		if credential.SessionIDIsValid(username, id){

			//We do it here for logging to console only. We parse the request in the handler later again
			request := r.URL.Path[1:]
			request = strings.Replace(request, "portal/", "", -1)

			currentTime := time.Now().Format(time.RFC1123)

			fmt.Println("Request : ", currentTime, r.Header.Get("X-Forwarded-For"), request, "("+username+")")
			fmt.Fprintf(w, strings.Replace(readHtmlFile("<center>"+web.Handler(r)+"<p><a class=\"btn btn-sm\" href=\"/portal\">Back</a><p></center>", " Welcome to GOpher Portal (logged in as "+username+")", true), "html/", "../html/", -1))

		} else {
			session.Options.MaxAge = -1
			session.Save(r,w)
			fmt.Fprintf(w, strings.Replace(readHtmlFile("<center>Session ID is invalid. Please log in again!Redirecting to <a class=\"btn-sm btn-primary\" href=\"/portal\">Portal</a>!<meta http-equiv=\"refresh\" content=\"2; url=/portal\" /></center>", " Welcome to GOpher Portal (logged in as "+username+")", true), "html/", "../html/", -1))
		}

	}

}


func getSession(r *http.Request) *sessions.Session{
	//TODO replace with secret
	session, _ := store.Get(r, "gopherportal")
	return session
}



//Is the target of the login form
//TODO maybe cleanse input to avoid SQL injection
func loginHandler(w http.ResponseWriter, r *http.Request){
	r.ParseForm()
	username := r.FormValue("username")
	password := r.FormValue("password")

	if credential.LoginIsValid(username, password){
		session := getSession(r)
		session.Values["username"] = username
		session.Values["sessionid"] = credential.GenerateSessionID(username)
		//Cookie lasts for one week
		session.Options.MaxAge= 86400 * 7
		session.Save(r, w)
		fmt.Fprintln(w, readHtmlFile("<center>Welcome " + username + ". You are logged in!Redirecting to <a class=\"btn-sm btn-primary\" href=\"/portal\">Portal</a>!<meta http-equiv=\"refresh\" content=\"1; url=/portal\" /></center>", "Portal logged in", true))
	} else {
		w.WriteHeader(401)
		fmt.Fprintln(w, readHtmlFile("<center>Username or password is incorrect!</center>", "Incorrect username or password", false))
	}


}



//Shuts down web server
func exitHandler(w http.ResponseWriter, r *auth.AuthenticatedRequest) {
	fmt.Fprintln(w, readHtmlFile("Shut down!", "Bye for now", false))
	os.Exit(0)

}

//Logs out user
func logOutHandler(w http.ResponseWriter, r *http.Request) {
	session := getSession(r)
	username := session.Values["username"].(string)
	id := session.Values["sessionid"].(string)
	session.Options.MaxAge=-1
	session.Save(r, w)
	credential.DeleteSessionID(username, id)
	fmt.Fprintln(w, readHtmlFile("<center>Logged out. Redirecting to <a class=\"btn-sm btn-primary\" href=\"/\">Home</a>!<meta http-equiv=\"refresh\" content=\"3; url=/\" /></center>", "Log out", false))
}

//Handles authentication (basic authentication)
func secret(user, realm string) string {

	currentTime := time.Now().Format(time.RFC1123)
	fmt.Println("Login attempt: ", currentTime, user)
	file, err := os.Open("htpasswd")

	if os.IsNotExist(err) {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		username := strings.Split(line, ":")[0]
		password := strings.Split(line, ":")[1]

		if user == username {
			return password
		}
	}

	return ""
}

//Handles /*
func welcomeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, readHtmlFile("<center><p>This is my private dashboard that I use to create and delete my Droplet.</p><p>Authorized users <b>only!</b></p><p>Continue to <a class=\"btn-sm btn-primary\" href=\"portal\">Portal!</a></p></center>", "Welcome to GOpher Portal!", false))
}

//Manipulates the index.html in /html
//Replaces $TITLE$, $VERSION$, $TEXT$, and $LOGOUT$
func readHtmlFile(text string, title string, showLogOut bool) string {
	file, err := os.Open("./html/index.html")

	if os.IsNotExist(err) {
		log.Println(err)
		return "404 Not found"
	}

	fi, _ := file.Stat()
	data := make([]byte, fi.Size())

	file.Read(data)
	file.Close()

	content := string(data)
	content = strings.Replace(content, "$TEXT$", text, -1)
	content = strings.Replace(content, "$TITLE$", title, -1)
	content = strings.Replace(content, "$VERSION$", web.Version, -1)
	if showLogOut{
		content = strings.Replace(content, "$LOGOUT$", "<a href=\"/logout\">Log Out</a>", -1)
	} else {
		content = strings.Replace(content, "$LOGOUT$", "", -1)
	}


	return content
}

//Handles /ip
func ipHandler(w http.ResponseWriter, r *auth.AuthenticatedRequest) {
	fmt.Fprintf(w, "%s", web.AuthHandler(r))
}

//Launches the web server and such
func main() {
	authenticator := auth.NewBasicAuthenticator("test", secret)
	http.HandleFunc("/portal/", portalHandler)
	http.HandleFunc("/stop", authenticator.Wrap(exitHandler))
	http.HandleFunc("/logout", logOutHandler)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/ip", authenticator.Wrap(ipHandler))
	http.HandleFunc("/", welcomeHandler)
	http.Handle("/html/", http.StripPrefix("/html/", http.FileServer(http.Dir("./html"))))

	err := http.ListenAndServe("127.0.0.1:8080", nil)
	if err != nil {
		log.Fatal(err)
	}
	os.Exit(0)
}

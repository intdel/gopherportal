package database

import (
	"database/sql"
	"log"
	"os"

	"strconv"

	_ "github.com/mattn/go-sqlite3"
)

var databaseFileName = "data.sql"
var firstRun = false
var prepared = false

var dropletsTableName = "droplets"
var SessionTableName = "sessions"
var UserTableName = "users"

//TODO CHANGE TO PERPARED STMT

//Checks if file exists. If not, creates file and will call dbFirstRun to create necessary tables
//Needs to be called by OpenDB ONLY
func PrepareDB() {

	if prepared == false {
		databaseFile, err := os.Open(databaseFileName)

		if os.IsNotExist(err) {
			databaseFile, err = os.Create(databaseFileName)
			if err != nil {
				log.Fatalln("Could not create database file! (" + err.Error() + ")")
			}
			firstRun = true
			dbFirstRun()
		}

		databaseFile.Close()
		prepared = true
	}
}

//Opens SQLite DB. Takes care of situation where database does not exist yet
//Needs to be called everytime when trying to access DB. Use defer db.Close() after this.
func OpenDB() *sql.DB {

	PrepareDB()

	db, err := sql.Open("sqlite3", databaseFileName)
	if err != nil {
		log.Fatalln("Could not open database file! (" + err.Error() + ")")
	}

	return db
}

//Used by PrepareDB to create tables.
func dbFirstRun() {

	if firstRun {

		db, err := sql.Open("sqlite3", databaseFileName)
		if err != nil {
			log.Fatalln("Could not open database file! (" + err.Error() + ")")
		}
		defer db.Close()

		hourCreateStmt := "create table " + dropletsTableName + " (id int, time int);"
		sessionCreateStmt := "create table " + SessionTableName + " (username text, id text);"
		userCreateStmt := "create table " + UserTableName + " (username text, password text);"

		_, err = db.Exec(hourCreateStmt)
		if err != nil {
			log.Fatalf("Could not create history table (" + err.Error() + ")")
		}
		firstRun = false

		_, err = db.Exec(sessionCreateStmt)
		if err != nil {
			log.Fatalf("Could not create session table (" + err.Error() + ")")
		}

		_, err = db.Exec(userCreateStmt)
		if err != nil {
			log.Fatalf("Could not create session table (" + err.Error() + ")")
		}

	}

}

//AddDroplet adds a droplet to database
func AddDroplet(id int, time int) {

	db := OpenDB()
	defer db.Close()

	hourStmt, err := db.Prepare("insert into " + dropletsTableName + " values(?, ?);")
	defer hourStmt.Close()

	_, err = hourStmt.Exec(strconv.Itoa(id), strconv.Itoa(time))
	if err != nil {
		log.Fatalf("Could not insert into " + dropletsTableName + " table (" + err.Error() + ")")
	}

}

//GetDropletHour returns our for droplet with ID id. It returns -2 if not found; -1 if it is infinitely long; or any other value for actual hours
func GetDropletHour(id int) int {

	hours := -2
	found := false

	db := OpenDB()
	defer db.Close()

	getHourStmt, err := db.Prepare("select time from " + dropletsTableName + " where id =?;")
	defer getHourStmt.Close()

	rows, err := getHourStmt.Query(strconv.Itoa(id))
	if err != nil {
		log.Println("Could not get droplet hour " + err.Error())
		return -2
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&hours)
		if err != nil {
			log.Println("Could not get droplet hour " + err.Error())
			return -2
		}
		if found == true {
			log.Println("Found multiple rows for same ID...")
		}
		found = true
	}

	if hours == -2 {
		log.Println("Could not find droplet hour...")
	}

	return hours
}

//UpdateDropletHour updates droplet hours with id id to currentHour + hourDelta
//If droplet is not in DB, then it will create it with time 0+newhour
//Returns true if a monitor needs to be created
func UpdateDropletHour(id int, hourDelta int) bool {

	db := OpenDB()
	defer db.Close()

	currentHour := GetDropletHour(id)
	createMonitor := false

	if currentHour == -2 {
		AddDroplet(id, 2)
		currentHour = 2
		createMonitor = true
	}

	newHours := currentHour + hourDelta

	updateStmt, err := db.Prepare("update " + dropletsTableName + " set time=? where id=?;")
	defer updateStmt.Close()

	_, err = updateStmt.Exec(strconv.Itoa(newHours), strconv.Itoa(id))

	if err != nil {
		log.Fatalln("Could not update droplet hour")
	}

	return createMonitor

}

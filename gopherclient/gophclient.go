package main

import (
	"bytes"
	"errors"
	"fastnet/shared/config"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

//Internal vars
var vpnIP string
var version = "GOpher Portal 2.0 CLIENT 02/03/2016 A"

var clientConfig = config.ReadClientConfig()

//Prints help by default
func main() {

	args := os.Args
	helpMessage := "GOpher Portal VPN - " + version + "\n\n   connect -- connect to vpn\ndisconnect -- disconnect from droplet without deleting it\n        ip -- list IPv4\n     nmcli -- Print nmcli command\n   openvpn -- used for OpenVPN server side"

	if len(args) > 1 {
		switch args[1] {

		case "connect":
			if getIP() {
				connectVPN()
			}
		case "disconnect":
			disconnectVPN()
		case "nmcli":
			if getIP() {
				printNmcli()
			}
		case "ip":
			if getIP() {
				fmt.Println(vpnIP)
			} else {
				fmt.Println("No IP address to show.")
			}
		case "openvpn":
			connectOpenVPN()

		default:
			fmt.Println(helpMessage)
		}

	} else {
		fmt.Println(helpMessage)
	}

}

//Prints out nmcli command that would be used to modify and connect
func printNmcli() {
	connectionEdit, connectionUp, connectionPassword := generateNmcli()

	if connectionEdit != "" && connectionUp != "" {
		fmt.Println("nmcli " + connectionEdit)
		fmt.Println("nmcli " + connectionPassword)
		fmt.Println("nmcli " + connectionUp)
	}

}

//Generates nmcli by getting IP and ID etc. Returns the two parts (connection edit nmcli command and connection up nmcli command)
func generateNmcli() (connectionEdit string, connectionUp string, connectionPassword string) {

	nmclitmp := clientConfig.Nmcli
	nmclitmp = strings.Replace(nmclitmp, "$IP$", vpnIP, -1)
	nmclitmp = strings.Replace(nmclitmp, "$ID$", clientConfig.NmcliID, -1)

	connectionEdit = nmclitmp
	connectionUp = "connection up id " + clientConfig.NmcliID
	connectionPassword = "connection modify id " + clientConfig.NmcliID + " vpn.secrets 'password = " + clientConfig.NmcliPassword + "'"
	return connectionEdit, connectionUp, connectionPassword
}

//Gets IP from server and launches NMCLi connect
func connectVPN() {
	connectionEdit, connectionUp, connectionPassword := generateNmcli()
	fmt.Println("Changing to new IP...")
	cmd := exec.Command("/bin/bash", "-c", "nmcli "+connectionEdit)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err := cmd.Run()
	fmt.Printf("%s\n", out.String())

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Setting password...")
	cmd = exec.Command("/bin/bash", "-c", "nmcli "+connectionPassword)
	cmd.Stdout = &out
	cmd.Stderr = &out
	err = cmd.Run()
	fmt.Printf("%s\n", out.String())

	fmt.Println("Connecting...")
	cmd = exec.Command("/bin/bash", "-c", "nmcli "+connectionUp)
	cmd.Stdout = &out
	cmd.Stderr = &out
	err = cmd.Run()
	fmt.Printf("%s\n", out.String())

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully connected to VPN.")

}

//Launches nmcli disconnect command
func disconnectVPN() {
	fmt.Println("Disconnecting...")
	cmd := exec.Command("/bin/bash", "-c", "nmcli con down "+clientConfig.NmcliID)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err := cmd.Run()
	fmt.Printf("%s\n", out.String())

	if err != nil {
		log.Println("WARNING: ", err)
	}
}

//Gets IP from server. Returns false if not found.
func getIP() bool {

	var client http.Client
	request, err := http.NewRequest("GET", clientConfig.ServerURL+"/ip", nil)

	if err != nil {
		log.Fatal(err)
	}

	request.SetBasicAuth(clientConfig.Username, clientConfig.AccessToken)

	response, err := client.Do(request)

	if err != nil {
		log.Fatal(err)
	}

	if response.StatusCode != 200 {
		log.Fatal("Failed to get IP address: HTTP response code ", response.StatusCode)
		return false
	}

	data, _ := ioutil.ReadAll(response.Body)

	vpnIP = string(data)

	//Avoids parsing of other stuff even if code is 200
	if len(vpnIP) > len("255.255.255.255") {
		log.Fatalln("Failed to get IP address: IP address malformed (maxlen=15)")
	}

	fmt.Println("Read IP address is: ", vpnIP)

	if vpnIP == "401 Unauthorized\n" {
		log.Fatal(errors.New("401 Unauthorized"))
	}

	if vpnIP == "-1" {
		return false
	}

	return true

}

//Used by OpenVPN client to replace $IP$ from template config file, save it as openvpn config file, and then connect by calling OpenVPNstartCmd
func connectOpenVPN() {
	configs := config.ReadClientConfig()

	fmt.Println("Changing to new IP...")
	if !getIP() {

		cmd := exec.Command("/bin/bash", "-c", configs.OpenVPNstopCmd)
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &out
		err := cmd.Run()
		fmt.Printf("%s\n", out.String())

		if err != nil {
			log.Fatal(err)
		}

		log.Fatal("Could not get IP address. Sent " + configs.OpenVPNstopCmd)
	}

	templateFile, err := os.Open(configs.OpenVPNTemplate)

	if err != nil {
		log.Fatal(err)
	}

	fi, _ := templateFile.Stat()

	data := make([]byte, fi.Size())

	templateFile.Read(data)

	templateFile.Close()

	output := strings.Replace(string(data), "$IP$", vpnIP, -1)

	outputFile, err := os.Create(configs.OpenVPNTarget)

	if err != nil {
		log.Fatal(err)
	}

	outputFile.WriteString(output)

	outputFile.Close()

	cmd := exec.Command("/bin/bash", "-c", configs.OpenVPNstartCmd)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err = cmd.Run()
	fmt.Printf("%s\n", out.String())

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully connected to VPN.")

}
